import React from 'react'
import {
  Flex,
  FlexItem,
  PageSection,
  Text,
  TextContent,
  Tooltip
} from '@patternfly/react-core'
import {
  DatabaseIcon
} from '@patternfly/react-icons'
// PropTypes
import PropTypes from 'prop-types'

const PanelTitle = (props) => {
  return (
    <PageSection
      style={{
        background: 'linear-gradient(AliceBlue,transparent)',
        backgroundColor: 'White'
      }}
    >
      <Flex key={`${props.title} title flex`}>
        <FlexItem key={`${props.title} title text flex item`}>
          <TextContent>
            <Text
              component="h1"
              style={{
                color: 'RoyalBlue',
                fontSize: '30px',
                display: 'inline-block',
                whiteSpace: 'pre-wrap'
              }}
            >
              {props.title}
            </Text>
            <Text
              component="p"
              style={{
                color: 'RoyalBlue',
                fontSize: '20px',
                whiteSpace: 'pre-wrap'
              }}
            >
              {props.subtext}
            </Text>
          </TextContent>
        </FlexItem>
        <FlexItem
            key={`${props.title} title icon flex item`}
            align={{ default: 'alignRight' }}
          >
            <DatabaseIcon
              key={'scan panel dbavail icon'}
              style={{
                fontSize: '20px',
                color: props.dbAvailable ? 'green' : 'grey'
              }}
            />
        </FlexItem>
        <FlexItem key={`${props.title} db avail text flex item`}>
        <Tooltip
          key={'scan panel dbavail tooltip'}
          content={props.dbAddInfo}
          trigger={
            props.dbAddInfo && props.dbAddInfo !== ''
              ? 'mouseenterfocus'
              : 'manual'
          }
          isVisible={false}
        >
          <TextContent key={'scan panel dbavail textcon'}>
            <Text
              key={'scan panel dbavail text'}
              component="p"
              style={{
                fontStyle: 'italic',
                fontSize: '20px',
                color: props.dbAvailable ? 'green' : 'grey'
              }}
            >
              {props.dbAvailable
                ? 'Config DB connected'
                : 'Config DB unavailable'
              }
            </Text>
          </TextContent>
          </Tooltip>
        </FlexItem>
      </Flex>
    </PageSection>
  )
}

PanelTitle.propTypes = {
  title: PropTypes.string.isRequired,
  subtext: PropTypes.string.isRequired,
  dbAvailable: PropTypes.bool.isRequired,
  dbAddInfo: PropTypes.string
}

export default PanelTitle
