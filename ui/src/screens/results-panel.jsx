import React, { useState } from 'react'
import {
  Flex,
  FlexItem,
  Page,
  PageSection,
  PageSectionVariants
} from '@patternfly/react-core'
// Components
import ResultsTable from '../components/results-table'
import ResultsView from '../components/results-view'
import PanelTitle from '../components/panel-title'
// Hooks
import useResultsList from '../hooks/useResultsList'
import useResultsLoader from '../hooks/useResultsLoader'
import useMenu from '../hooks/useResultsMenu'
// Proptypes
import PropTypes from 'prop-types'

const ResultsPanel = (props) => {
  // States and custom Hooks
  const [resultsList, updateResultsList] = useResultsList(props.viewerUrl, props.dbAvailable, props.onError)
  const [
    curResultsData,
    curResultsTag,
    curResultsPaths,
    loadResultJson
  ] = useResultsLoader(props.viewerUrl, props.onError)

  const [showMenu, setshowMenu] = useState('')
  const [showResults, setshowResults] = useState('')
  // const [selectedScan, setselectedScan] = useState('')
  const [selectedMenuItems, setSelectedMenuItems, menuCheckboxList] = useMenu(curResultsPaths)

  // React components
  const title = (
    <PanelTitle
      title={'Result Viewer'}
      subtext={'Display the histograms from scan results stored in the database '}
      dbAvailable={props.dbAvailable}
      dbAddInfo={`DB URL: ${props.dbUrl}`}
    />
  )

  const getResults = (row) => {
    console.log(row)
    setshowMenu(true)
    setshowResults(true)
    setSelectedMenuItems([])
    loadResultJson(row[1], props.viewerUrl)
  }

  const updateList = () => {
    // setshowMenu(false) do I want this here?
    updateResultsList()
  }

  const resultsTable = (
    <ResultsTable
      resultsList={resultsList}
      scanTypes={props.scanTypes}
      onRowClick={(row) => getResults(row)}
      updateResultsList={updateList}
      toggleMenu={setshowMenu}
      toggleResults={setshowResults}
    />
  )

  const resultsView = (
    <ResultsView
      curResultsData={curResultsData}
      curResultsTag={curResultsTag}
      curResultsPaths={curResultsPaths}
      selectedMenuItems={selectedMenuItems}
      viewerUrl={props.viewerUrl}
    />
  )

  const resultsViewPanel = (
    <PageSection
      isFilled
      variant={PageSectionVariants.light}
    >
      <Flex>
        <Flex
          flex={{ default: 'flex_2' }}
          grow={{ default: 'grow' }}
        >
          <FlexItem>
            {resultsTable}
            {showMenu ? menuCheckboxList() : null}
          </FlexItem>
        </Flex>
        <Flex
            direction={{ default: 'column' }}
            grow={{ default: 'grow' }}
            flex={{ default: 'flex_3' }}
          >
          <FlexItem>
          {showResults ? resultsView : null}
          </FlexItem>
        </Flex>
      </Flex>
    </PageSection>
  )

  return (
    <Page>
    {title}
    {resultsViewPanel}
    </Page>
  )
}

ResultsPanel.propTypes = {
  scanTypes: PropTypes.array.isRequired,
  dbAvailable: PropTypes.bool.isRequired,
  viewerUrl: PropTypes.string.isRequired,
  dbUrl: PropTypes.string.isRequired,
  onError: PropTypes.func.isRequired,
  onWarn: PropTypes.func.isRequired,
  onInfo: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired
}

export default ResultsPanel
