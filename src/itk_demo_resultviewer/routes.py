from itk_demo_resultviewer.helper_functions import create_valid_json, create_tbufferjson
from itk_demo_resultviewer.Viewer_FSM.context import Context
from datetime import datetime, timedelta
from rcf_response import Info, Error
from connexion import request
from requests import post,get
from service_registry import ServiceRegistry
from service_registry.exceptions import SRException
from .Resultviewer_Config import (
  ETCD_HOST,
  ETCD_PORT,
  ETCD_NAMESPACE,
  CONFIGDB_ADDRESS,
  CONFIGDB_URL_KEY,
  START_NGINX
) 
import logging
import glob
import json
import sys
import os

logging.basicConfig(level=logging.INFO)
headers = {'content-type': 'application/json', 'accept': 'application/json'}

config_db_address = None
 
# Search for Config DB
if CONFIGDB_URL_KEY is not None:
    try:
        sr = ServiceRegistry()
        get_configdb_url, end_watch = sr.watch_value(CONFIGDB_URL_KEY, CONFIGDB_ADDRESS)
        config_db_address = get_configdb_url()
    except SRException as e:
        sr = None
        print(f"Warning: Service registry is not available: {e}", file=sys.stderr)

elif config_db_address is None and CONFIGDB_ADDRESS is not None:
    config_db_address = CONFIGDB_ADDRESS

# try:
#     sr = ServiceRegistry(
#         namespace = ETCD_NAMESPACE,
#         etcd_host = ETCD_HOST,
#         etcd_port = ETCD_PORT
#     )
#     config_dbs = sr.get_all_services(service_type = "itk-demo-configdb")
# except SRException as e:
#     sr = None
#     config_dbs = []
#     print(f"Warning: Service registry is not available: {e}", file=sys.stderr)

# if len(config_dbs) > 0:
#     headers = {"content-type": "application/json", "accept": "application/json"}
#     try:
#         config_db_host = config_dbs[0]["hostname"]
#         config_db_port = sr.get_service_key(
#             "port",
#             config_dbs[0]["service_type"],
#             config_dbs[0]["service_name"],
#             config_db_host
#         )
#         config_db_apiPrefix = sr.get_service_key(
#             "apiPrefix",
#             config_dbs[0]["service_type"],
#             config_dbs[0]["service_name"],
#             config_db_host
#         )
#         config_db_address = f"http://{config_db_host}:{config_db_port}{config_db_apiPrefix}"
#     except SRException as e:
#         print(
#             f"Failed to obtain Config DB info from the service registry: {e}",
#             file=sys.stderr
#         )
# elif CONFIGDB_ADDRESS is not None:
#   config_db_address = CONFIGDB_ADDRESS
# else:
#   config_db_address = None
# del sr

context = Context(config_db_address)

def _configDbAvailable():
  global config_db_address
  if config_db_address is None:
    return False
  try:
    result = get(
      f"{config_db_address}/health",
      headers=headers
    ).json()
    if not result["status"] == 200:
      return False
    return True
  except:
    return False


def health():
  output = Info()
  global config_db_address
  output.payload["dbAvailable"] = _configDbAvailable()
  output.payload["dbUrl"] = "" if config_db_address is None else config_db_address

  return output.to_conn_response()

# endpoint for the useRunkeyTree hook
def db_get_tag():
  global context
  params = request.json
  return context.state.db_get_tag(context,params).to_conn_response()

def db_get_all():
  global context
  params = request.json
  return context.state.db_get_all(context,params).to_conn_response()

def cache_json():
  global context
  params = request.json  
  return context.state.cache_json(context,params).to_conn_response()
