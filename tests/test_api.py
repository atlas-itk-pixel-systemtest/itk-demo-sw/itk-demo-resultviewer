from tests.parent_test import ParentTest
from itk_demo_resultviewer.helper_functions import create_valid_json
import httpretty
import pytest
import json
import re

class TestExamples(ParentTest):

#     def test_health(self):
#         result = self.get("health") 
#         assert result["data"] == "OK"

#     def test_post(self):
#         dic = {"data":"Hello"}
#         result = self.post(dic, "post_example") 
#         assert result["data"] == "Hello back!"

#     def test_post_missing_data(self):
#         dic = {}
#         result = self.post(dic, "post_example") 
#         assert result["data"] == "'data' is a required property"

#     def test_404(self):
#         result = self.get("test") 
#         assert result["data"] == "The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again."

    @pytest.mark.endpoint
    @httpretty.activate
    def test_db_get_all(self,mock_db_get_all_scans):

        httpretty.register_uri(
            httpretty.POST,
            "http://mock_db/get_all",
            body=json.dumps(mock_db_get_all_scans)
        )

        req_body = {
            "dbUrl" : "http://mock_db",
            "verbosity" : 1,
            "table" : "scan"
        }

        # res = self.client.post(
        #     "/db_get_all", data=json.dumps(req_body), headers={"content-type": "application/json"}
        # )
        # scans_list = json.loads(res.data)
        scans_list = self.post(req_body, "db_get_all")

        used_entries = ["id","runkey_id","time","type"]

        assert type(scans_list) == dict
        # assert "test" in scans_list.keys()

        for entry in used_entries:
            assert entry in scans_list["payload"]["list"].keys()



@pytest.mark.internal
def test_create_valid_json():
    invalid_json = '{"This" : "string"}{"is" : "no" , "valid" : "json"}'
    valid_jsons = create_valid_json(invalid_json, "}{", "{", "}")
    success = True
    for entry in valid_jsons:
        try: 
            json.loads(entry)
        except ValueError as e:
            success = False

    assert success







