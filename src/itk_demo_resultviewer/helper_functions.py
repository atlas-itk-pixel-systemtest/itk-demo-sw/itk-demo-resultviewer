import json

# All 1D histos I found in the yarr StdAnalysis.cpp
histos1D = ["L1Dist", "HitsPerEvent", "TotMap", "MeanTotDist", "SigmaTotDist", "MeanTotDistFine", 
            "avgTotVsCharge", "Scurve", "Chi2Dist", "StatusDist", "TimePerFitDist","ThresholdDist",
            "NoiseDist","responseCurve","OccupancyDist","L1Dist","TagDist","TotDist","Delay"]

# Itk-felix-sw results are saved as multiple json objects in one string, 
# this function transforms that string into one valid json object
def create_valid_json(s, delimiter, prefix, suffix):
    split = s.split(delimiter)
    if len(split) == 1:
        return split
    split_delimiter = [prefix + substr +
                       suffix for substr in split[:-1]] + [split[-1]]
    split_delimiter[0] = split_delimiter[0][1:]
    split_delimiter[-1] = prefix+split_delimiter[-1]
    return split_delimiter


# Yarr results have their own format, this function
# transforms them into the format needed by jsroot
def create_tbufferjson(data):
    results = []
    data = json.loads(data)
    for histName in data[0].keys():
        hist1D = False
        histNameShortened = histName.split('-')[0]
        if (histNameShortened in histos1D):
            hist1D = True
        if (hist1D):
            results_histName = {
                "_typename": "TH1D",
                "fBits": 8,
                "fName": data[0][histName]["Name"],
                "fTitle": data[0][histName]["Name"],
                "fLineColor": 50,
                "fLineStyle": 1,
                "fLineWidth": 2,
                "fFillColor": 50,
                "fXaxis": {
                    "_typename": "TAxis",
                    "fBits": 0,
                    "fTitle": "Column",
                    "fNdivisions": 510,
                    "fAxisColor": 1,
                    "fLabelFont": 42,
                    "fLabelOffset": 0.005,
                    "fLabelSize": 0.035,
                    "fTickLength": 0.03,
                    "fTitleOffset": 1,
                    "fTitleSize": 0.035,
                    "fTitleFont": 42,
                    "fNbins": data[0][histName]["x"]["Bins"],
                    "fXmin": data[0][histName]["x"]["Low"],
                    "fXmax": data[0][histName]["x"]["High"],
                    "fXbins": []
                },
                "fYaxis": {
                    "_typename": "TAxis",
                    "fBits": 0,
                    "fTitle": data[0][histName]["y"]["AxisTitle"],
                    "fNdivisions": 510,
                    "fAxisColor": 1,
                    "fLabelFont": 42,
                    "fLabelOffset": 0.005,
                    "fLabelSize": 0.035,
                    "fTickLength": 0.03,
                    "fTitleOffset": 0,
                    "fTitleSize": 0.035,
                    "fTitleFont": 42,
                    "fNbins": 1,
                    "fXmin": 0,
                    "fXmax": 1,
                    "fXbins": []
                },
                "fZaxis": {
                    "_typename": "TAxis",
                    "fBits": 0,
                    "fNdivisions": 510,
                    "fAxisColor": 1,
                    "fLabelColor": 1,
                    "fLabelFont": 42,
                    "fLabelOffset": 0.005,
                    "fLabelSize": 0.035,
                    "fTickLength": 0.03,
                    "fTitleOffset": 1,
                    "fTitleSize": 0.035,
                    "fTitleFont": 42,
                    "fNbins": 1,
                    "fXmin": 0,
                    "fXmax": 1,
                    "fXbins": []
                },
                "fMaximum": -1111,
                "fMinimum": -1111,
                "fBufferSize": 0,
                "fBuffer": [],
            }
            nxBins = results_histName["fXaxis"]["fNbins"]
            # 2 overflow bins at index 0 and nxBins+1
            results_histName["fArray"] = [0]*(nxBins + 2)
            # this is necessary for 1D histograms
            results_histName["fSumw2"] = [0]*(nxBins + 2)
            for i in range(1, nxBins + 1):
                results_histName["fArray"][i] = data[0][histName]["Data"][i-1]
                results_histName["fSumw2"][i] = data[0][histName]["Data"][i-1]

        else:
            results_histName = {
                "_typename": "TH2I",
                "fBits": 8,
                "fName": data[0][histName]["Name"],
                "fTitle": data[0][histName]["Name"],
                "fXaxis": {
                    "_typename": "TAxis",
                    "fBits": 0,
                    "fTitle": "Column",
                    "fNdivisions": 510,
                    "fAxisColor": 1,
                    "fLabelFont": 42,
                    "fLabelOffset": 0.005,
                    "fLabelSize": 0.035,
                    "fTickLength": 0.03,
                    "fTitleOffset": 1,
                    "fTitleSize": 0.035,
                    "fTitleFont": 42,
                    "fNbins": data[0][histName]["x"]["Bins"],
                    "fXmin": data[0][histName]["x"]["Low"],
                    "fXmax": data[0][histName]["x"]["High"],
                    "fXbins": []
                },
                "fYaxis": {
                    "_typename": "TAxis",
                    "fBits": 0,
                    "fTitle": "Row",
                    "fNdivisions": 510,
                    "fAxisColor": 1,
                    "fLabelFont": 42,
                    "fLabelOffset": 0.005,
                    "fLabelSize": 0.035,
                    "fTickLength": 0.03,
                    "fTitleOffset": 0,
                    "fTitleSize": 0.035,
                    "fTitleFont": 42,
                    "fNbins": data[0][histName]["y"]["Bins"],
                    "fXmin": data[0][histName]["y"]["Low"],
                    "fXmax": data[0][histName]["y"]["High"],
                    "fXbins": []
                },
                "fZaxis": {
                    "_typename": "TAxis",
                    "fBits": 0,
                    "fNdivisions": 510,
                    "fAxisColor": 1,
                    "fLabelColor": 1,
                    "fLabelFont": 42,
                    "fLabelOffset": 0.005,
                    "fLabelSize": 0.035,
                    "fTickLength": 0.03,
                    "fTitleOffset": 1,
                    "fTitleSize": 0.035,
                    "fTitleFont": 42,
                    "fNbins": 1,
                    "fXmin": 0,
                    "fXmax": 1,
                    "fXbins": []
                },
                "fMaximum": -1111,
                "fMinimum": -1111,
                "fBufferSize": 0,
                "fBuffer": []
            }

            nxBins = results_histName["fXaxis"]["fNbins"]
            nyBins = results_histName["fYaxis"]["fNbins"]
            nBins = nxBins * nyBins
            # Those bins 'surround' the actual histogram at index 0 and max+1
            nOverflow = 2*nxBins + 2*nyBins + 4
            results_histName["fArray"] = [0]*(nBins + nOverflow)

            for i in range(1, nBins + nOverflow + 1):
                # https://root.cern.ch/doc/master/classTH1.html#a55e591270aaad37c3059a62f83566e4e
                xBin = i % (nxBins+2)
                yBin = ((i-xBin)/(nxBins+2)) % (nyBins+2)
                # Overflow and Underflow Bins, not saved in yarr histo
                if (xBin < nxBins+1 and xBin > 0 and yBin > 0 and yBin < nyBins + 1):
                    results_histName["fArray"][i - 1] = data[0][histName]["Data"][int(xBin)-1][int(yBin)-1]
                else:
                    results_histName["fArray"][i-1] = 0.0
        results.append(json.dumps(results_histName))

    return (results)
