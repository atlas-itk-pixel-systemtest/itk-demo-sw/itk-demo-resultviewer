import { useState } from 'react'
import { generalRequest } from '@itk-demo-sw/utility-functions'

export default function useResultLoader (
  viewerUrl,
  onError = (data) => { }
) {
  const [resultData, setResultData] = useState('')
  const [resultTag, setResultTag] = useState('')
  const [resultPaths, setResultPaths] = useState('[]')

  const _getResultId = async (tagOrId) => {
    if (!isNaN(parseInt(tagOrId))) {
      setResultTag('')
      return tagOrId
    }
    const reqBody = {
      stage: false,
      metadata: false,
      name: tagOrId,
      table: 'results'
    }
    return generalRequest(
      `${viewerUrl}/db_get_tag`, reqBody
    ).then(
      data => {
        setResultTag(tagOrId)
        return data.payload.dataset
      }
    ).catch(
      err => {
        setResultData('Error loading config tag!')
        onError(err)
      }
    )
  }

  // This one saves the json to /dev/shm and returns the path
  const loadResultJson = (tagOrId, viewerUrl) => {
    console.log('LoadResultJson')
    console.log(tagOrId)
    _getResultId(tagOrId).then(
      (id) => {
        const reqBody = {
          id: id
        }
        generalRequest(
          `${viewerUrl}/cache_json`, reqBody
        ).then(
          data => {
            const Paths = []
            for (let i = 0; i < data.payload.jsonPaths.length; i++) {
              Paths.push(data.payload.jsonPaths[i])
            }
            setResultPaths(Paths)
          }
        ).catch(
          err => {
            setResultData('Error loading results!')
            onError(err)
          }
        )
      }
    )
  }

  return [resultData, resultTag, resultPaths, loadResultJson]
}
