import React, { useState } from 'react'
import {
  Flex,
  FlexItem,
  Page,
  PageSection,
  PageSectionVariants
} from '@patternfly/react-core'
// Screens
// Components
import RunkeyView from '../components/runkey-view'
import ResultsView from '../components/results-view'
import PanelTitle from '../components/panel-title'
// Hooks
import useRunkeyList from '../hooks/useRunkeyList'
import useResultsLoader from '../hooks/useResultsLoader'
import useMenu from '../hooks/useResultsMenu'
// PropTypes
import PropTypes from 'prop-types'
import useResultsList from '../hooks/useResultsList'

const RunkeyPanel = (props) => {
  // States and custom Hooks
  // Get The RunkeyList
  const [runkeyList, updateRunkeys] = useRunkeyList(props.viewerUrl, props.dbAvailable, props.onError)
  const [resultsList, updateResultsList] = useResultsList(props.viewerUrl, props.dbAvailable, props.onError)
  // Get the List of Results
  const [
    curResultsData,
    curResultsTag,
    curResultsPaths,
    loadResultJson
  ] = useResultsLoader(props.viewerUrl, props.onError)

  // Show The Checkboxmenu when selecting a Result
  const [showMenu, setshowMenu] = useState('')
  const [showResults, setshowResults] = useState('')
  const [selectedMenuItems, setSelectedMenuItems, menuCheckboxList] = useMenu(curResultsPaths)

  // Get a Reslut when clicked on
  const getResults = (row) => { // Gets Called when selectiong an result: See ResultsPanel
    console.log(row)
    setshowMenu(true)
    setshowResults(true)
    setSelectedMenuItems([])
    loadResultJson(row, props.viewerUrl)
  }

  // React Components
  const title = (
    <PanelTitle
      title={'Result Viewer'}
      subtext={'Display the histograms from scan results stored in the database '}
      dbAvailable={props.dbAvailable}
      dbAddInfo={`DB URL: ${props.dbUrl}`}
    />
  )

  const runkeyView = (
    <RunkeyView
      viewerUrl={props.viewerUrl}
      onError={props.onError}
      runkeyList={runkeyList}
      updateRunkeys={() => {
        updateRunkeys()
        updateResultsList()
      }}
      onCompSelect={(row) => getResults(row)}
      toggleMenu={setshowMenu}
      toggleResults={setshowResults}
      results={resultsList}
    />
  )

  const resultsView = (
    <ResultsView
      curResultsData={curResultsData}
      curResultsTag={curResultsTag}
      curResultsPaths={curResultsPaths}
      selectedMenuItems={selectedMenuItems}
      viewerUrl={props.viewerUrl}
    />
  )

  return (
    <Page>
      {title}
      <PageSection
        isFilled
        variant={PageSectionVariants.light}
      >
        <Flex
        >
          <Flex
            flex={{ default: 'flex_2' }}
            grow={{ default: 'grow' }}
          >
            <FlexItem>
              {runkeyView}
              {showMenu ? menuCheckboxList() : null}
            </FlexItem>
          </Flex>
          <Flex
            direction={{ default: 'column' }}
            grow={{ default: 'grow' }}
            flex={{ default: 'flex_3' }}
          >
            <FlexItem>
              {showResults ? resultsView : null}
            </FlexItem>
          </Flex>
        </Flex>
      </PageSection>
    </Page>
  )
}

RunkeyPanel.propTypes = {
  dbUrl: PropTypes.string.isRequired,
  onError: PropTypes.func.isRequired,
  dbAvailable: PropTypes.bool.isRequired,
  viewerUrl: PropTypes.string.isRequired
}

export default RunkeyPanel
