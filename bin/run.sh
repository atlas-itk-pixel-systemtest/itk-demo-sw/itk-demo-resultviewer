#!/usr/bin/env bash

# GUNICORN
# demi python run 
gunicorn wsgi:app --bind 0.0.0.0:5010 --workers=1 --reload
# --log-level=debug
# --workers=$(nproc)

# uWSGI
# uwsgi --http :9090 --wsgi-file wsgi.py --callable app
