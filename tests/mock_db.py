get_all_scans_response = {
                            "limit": 10,
                            "list": {
                                "config_id": [
                                12,
                                12,
                                12,
                                12,
                                126,
                                129,
                                12,
                                12,
                                136,
                                12
                                ],
                                "id": [
                                47,
                                48,
                                49,
                                50,
                                51,
                                52,
                                53,
                                54,
                                55,
                                56
                                ],
                                "runkey_id": [
                                51,
                                52,
                                53,
                                54,
                                55,
                                56,
                                57,
                                58,
                                59,
                                60
                                ],
                                "time": [
                                "2022-08-03T08:24:41.196029Z",
                                "2022-08-03T08:33:18.312996Z",
                                "2022-08-03T09:25:09.477151Z",
                                "2022-08-03T09:31:41.969295Z",
                                "2022-08-03T09:34:26.673077Z",
                                "2022-08-03T09:36:13.552299Z",
                                "2022-08-03T09:38:49.117858Z",
                                "2022-08-03T09:40:58.061444Z",
                                "2022-08-03T09:41:43.664251Z",
                                "2022-08-03T13:13:44.030876Z"
                                ],
                                "type": [
                                "digitalscan",
                                "digitalscan",
                                "digitalscan",
                                "digitalscan",
                                "analogscan",
                                "totscan",
                                "digitalscan",
                                "digitalscan",
                                "totscan",
                                "digitalscan"
                                ]
                            },
                            "offset": 46,
                            "order_by": "id",
                            "status": 200,
                            "status_id": 6141,
                            "table": "scan",
                            "verbosity": 1
                        }

