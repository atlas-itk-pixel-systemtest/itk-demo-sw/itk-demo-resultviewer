import React, { useState, useEffect } from 'react'
import { useConfig } from '../admin-panel/config'
import {
  Alert,
  Bullseye,
  Page,
  PageSection,
  PageSectionVariants,
  PageNavigation,
  Spinner
} from '@patternfly/react-core'
import { generalRequest } from '@itk-demo-sw/utility-functions'
// Screens
import ResultsPanel from './results-panel'
import RunkeyPanel from './runkey-panel'
import MinimalRootPanel from './root-panel-minimal'
import FillerPanelNoDB from './filler-panel-nodb'
import FillerPanelNoAPI from './filler-panel-noapi'
// Components
import {
  Navbar,
  Notifications
} from '@itk-demo-sw/components'
// Hooks
import {
  useConfigureValue,
  useLoggingViewer,
  useNavbar,
  useNotifications
} from '@itk-demo-sw/hooks'

const ResultsDashboard = (props) => {
  const [dbUrl, setDbUrl] = useState('')
  const [initialLoad, setInitialLoad] = useState(true)
  const [dbAvailable, setDbAvailable] = useState(false)
  const [backendAvailable, setBackendAvailable] = useState(false)
  const [connectionEstablished, setConnectionEstablished] = useState(false)
  const [logContent, addLog] = useLoggingViewer()
  const { getConfig } = useConfig()
  const healthFrequency = getConfig('healthFrequency')
  const callHealth = getConfig('callHealth')
  const [intervalId, setIntervalId] = useState(0)

  const [
    alerts,
    onError,
    onWarn,
    onInfo,
    onSuccess,
    deleteAlert
  ] = useNotifications(addLog)

  const viewerUrl = useConfigureValue('/config', 'urlKey', getConfig('backend'), 'backendUrl')

  const scanTypes = [
    'DigitalScan',
    'AnalogScan',
    'ThresholdScan',
    'TestScan',
    'SensorScan',
    'ConfigScan',
    'TotScan',
    'PreampTune',
    'MaskCheck',
    'GlobalThresholdTune',
    'PixelThresholdTune'
  ]

  const panels = [
    {
      title: 'Results per Runkey',
      content: <RunkeyPanel
        dbUrl={dbUrl}
        viewerUrl={viewerUrl}
        dbAvailable={dbAvailable}
        onError={onError}
        onWarn={onWarn}
        onInfo={onInfo}
        onSuccess={onSuccess}
      />
    },
    {
      title: 'All results from database',
      content: <ResultsPanel
        scanTypes={scanTypes}
        viewerUrl={viewerUrl}
        dbAvailable={dbAvailable}
        dbUrl={dbUrl}
        onError={onError}
        onWarn={onWarn}
        onInfo={onInfo}
        onSuccess={onSuccess}
        logContent={logContent}
      />
    },
    {
      title: 'Display .root files',
      content: <MinimalRootPanel/>
      // content: <JsrootPanel
      //   viewerUrl={viewerUrl}
      //   dbAvailable={dbAvailable}
      //   onError={onError}
      //   onWarn={onWarn}
      //   onInfo={onInfo}
      //   onSuccess={onSuccess}
      //   logContent={logContent}
      // />
    }
  ]

  const panelsNoAPI = [
    {
      title: 'Results per Runkey',
      content: <FillerPanelNoAPI
        dbAvailable={dbAvailable}
      />
    },
    {
      title: 'All results from database',
      content: <FillerPanelNoAPI
        dbAvailable={dbAvailable}
      />
    },
    {
      title: 'Display .root files',
      content: <MinimalRootPanel/>
    }
  ]

  const panelsNoDb = [
    {
      title: 'Results per Runkey',
      content: <FillerPanelNoDB
        dbAvailable={dbAvailable}
      />
    },
    {
      title: 'All results from database',
      content: <FillerPanelNoDB
        dbAvailable={dbAvailable}
      />
    },
    {
      title: 'Display .root files',
      content: <MinimalRootPanel/>
    }
  ]

  const [activeItem, itemNames, changePanel] = useNavbar(
    panels.map((p) => p.title)
  )

  const handleLog = response => {
    if ((Object.keys(response).includes('status')) &&
      (Object.keys(response).includes('messages'))) {
      if (response.messages.length > 0) {
        for (const m of response.messages) {
          if (response.status === 200) onInfo(m)
          if (response.status === 299) onWarn(m)
        }
      }
    }
  }

  const getHealth = (viewerUrl) => {
    console.log(viewerUrl)
    generalRequest(`${viewerUrl}/health`).then(
      data => {
        handleLog(data)
        setDbAvailable(data.payload.dbAvailable)
        setDbUrl(data.payload.dbUrl)
        // console.log(data)
        // console.log(data.payload.dbUrl)
        setInitialLoad(false)
        setConnectionEstablished(true)
        setBackendAvailable(true)
      }
    ).catch(
      (err) => {
        // onError(err)
        console.log(err)
        console.log('Failed flask backend connection. URL: ' + viewerUrl)
        setInitialLoad(false)
        setBackendAvailable(false)
      }
    )
  }

  useEffect(() => {
    clearInterval(intervalId)
    getHealth(viewerUrl)
    setIntervalId(setInterval(() => { if (callHealth) getHealth(viewerUrl) }, healthFrequency))
  }, [callHealth, healthFrequency, viewerUrl])

  const noConnectionAlert = (
    connectionEstablished
      ? null
      : (
        <PageSection
          key={'no connection alert sec'}
          variant={PageSectionVariants.light}
          style={{ padding: '0px' }}
        >
          <Alert
            key={'no connection alert alert'}
            variant="danger"
            isInline
            title="DAQ API is not responding!"
          >
            {'Please check if the API backend is running. ' +
            `Currently trying to connect to "${
              viewerUrl !== ''
                ? viewerUrl
                : location.host
            }".`}
          </Alert>
        </PageSection>
        )
  )

  const loadingPage = (
    <PageSection
      key={'loading page section'}
      variant={PageSectionVariants.light}
      sticky='top'
      isFilled
    >
      <Bullseye key={'loading bullseye'}>
        <Spinner
          isSVG
          key={'loading spinner'}
          diameter="80px"
        />
      </Bullseye>
    </PageSection>
  )

  const content = (
    <>
      <PageSection padding={{ default: 'noPadding' }}>
        <PageNavigation>
          <Navbar
            activeItem={activeItem}
            changePanel={changePanel}
            itemNames={itemNames}
            onError={onError}
            onInfo={onInfo}
            onSuccess={onSuccess}
            onWarn={onWarn}
          />
        </PageNavigation>

      </PageSection>
      <Notifications alerts={alerts} deleteAlert={deleteAlert}/>
      <PageSection
        isFilled
        hasOverflowScroll
        padding={{ default: 'noPadding' }}
      >
        {dbAvailable ? panels[activeItem].content : backendAvailable ? panelsNoDb[activeItem].content : panelsNoAPI[activeItem].content}
      </PageSection>
    </>
  )

  return (
    <Page>
      {backendAvailable || initialLoad ? null : noConnectionAlert}
      {initialLoad ? loadingPage : content}
    </Page>
  )
}

export default ResultsDashboard
