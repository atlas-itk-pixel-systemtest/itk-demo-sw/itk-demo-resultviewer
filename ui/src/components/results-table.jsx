import React, { useState, useEffect } from 'react'
import {
  Bullseye,
  Button,
  Divider,
  EmptyState,
  EmptyStateBody,
  EmptyStateIcon,
  EmptyStateSecondaryActions,
  Pagination,
  Title,
  Toolbar,
  ToolbarContent,
  ToolbarFilter,
  ToolbarItem
} from '@patternfly/react-core'
import {
  SearchIcon,
  SyncIcon
} from '@patternfly/react-icons'
// Components
import {
  CheckboxSelect,
  SearchField,
  StandardTable
} from '@itk-demo-sw/components'
// Hooks
import {
  useSearchField,
  useStandardTable
} from '@itk-demo-sw/hooks'
import useCustomCheckboxSelect from '../hooks/useCustomCheckboxSelect'
import useCustomPagination from '../hooks/useCustomPagination'
// PropTypes
import PropTypes from 'prop-types'

const ResultsTable = (props) => {
  // States and custom Hooks
  const [nameFilter, setNameFilter] = useState('')
  const [selectedRow, setselectedRow] = useState([])

  const [
    searchValue,
    handleSearchChange,
    handleSearch,
    clearSearch
  ] = useSearchField(() => setNameFilter(searchValue))

  const [
    isOpen,
    selections,
    onToggle,
    onSelect,
    clearSelections
  ] = useCustomCheckboxSelect(props.toggleMenu, props.toggleResults, setselectedRow)

  const [
    page,
    perPage,
    handlePerPageSelect,
    resetPage,
    onSetPage,
    onFirstClick,
    onNextClick,
    onPreviousClick,
    onLastClick
  ] = useCustomPagination(15, setselectedRow)

  const [filteredRows, setFilteredRows] = useStandardTable(props.resultsList)

  // Helper functions

  const applyFilters = () => {
    const newRows = (
      nameFilter !== '' || selections.length > 0
        ? props.resultsList.filter(row => {
          return (
            (nameFilter === '' ||
            String(row[1]) === nameFilter
            ) &&
            (selections.length === 0 || selections.includes(row[2]))
          )
        })
        : props.resultsList
    )
    setFilteredRows([...newRows])
    resetPage()
  }

  const clearAll = () => {
    clearSearch()
    setNameFilter('')
    clearSelections()
  }

  const handleDelete = (filterOption, id = '') => {
    if (filterOption === 'Scan ID') {
      clearSearch()
      setNameFilter('')
    }
    if (filterOption === 'Scan Type') {
      if (selections.includes(id)) {
        onSelect(null, id)
      }
    }
    resetPage()
  }

  // Additional Hooks
  useEffect(() => {
    applyFilters()
  }, [nameFilter, selections, props.resultsList])

  // Constants
  const perPageOptions = [
    { title: '5', value: 5 },
    { title: '10', value: 10 },
    { title: '15', value: 15 },
    { title: '20', value: 20 }
  ]

  const columns = [
    // { title: 'ID' },
    { title: 'Runkey' },
    { title: 'Scan ID' },
    { title: 'Scan Type' },
    { title: 'Date' }
    // { title: 'User' }
  ]

  const doSearch = () => {
    props.toggleMenu(false)
    props.toggleResults(false)
    handleSearch()
  }

  const doRowClick = (row) => {
    props.onRowClick(row)
    setselectedRow([filteredRows.slice((page - 1) * perPage, page * perPage).indexOf(row)])
  }

  // React components
  const searchField = (
    <SearchField
      handleChange={handleSearchChange}
      handleSearch={doSearch}
      value={searchValue}
      name={'Scan ID'}
    />
  )

  const checkboxSelect = (
    <CheckboxSelect
      isOpen={isOpen}
      onToggle={onToggle}
      onSelect={onSelect}
      options={props.scanTypes}
      selections={selections}
      name={'Scan Type'}
    />
  )

  const refreshButton = (
    <Button
      variant="plain"
      onClick={() => props.updateResultsList()}
    >
      <SyncIcon />
    </Button>
  )

  const pagination = (
    <Pagination
      itemCount={filteredRows.length}
      perPage={perPage}
      page={page}
      perPageOptions={perPageOptions}
      onPerPageSelect={handlePerPageSelect}
      onSetPage={onSetPage}
      onFirstClick={() => { onFirstClick(perPage) }}
      onNextClick={() => { onNextClick(page, perPage) }}
      onPreviousClick={() => { onPreviousClick(page, perPage) }}
      onLastClick={() => { onLastClick(filteredRows.length, perPage) }}
      // isCompact
    />
  )

  const toolbar = (
    <Toolbar
      id="results-list-toolbar"
      clearAllFilters={clearAll}
      collapseListedFiltersBreakpoint="xl"
    >
      <ToolbarContent>
        <ToolbarFilter
          variant="search-filter"
          categoryName="Scan ID"
          chips={nameFilter === '' ? [] : [nameFilter]}
          deleteChip={handleDelete}
        >
          {searchField}
        </ToolbarFilter>
        <ToolbarFilter
          categoryName="Scan Type"
          chips={selections}
          deleteChip={handleDelete}
        >
          {checkboxSelect}
        </ToolbarFilter>
        <ToolbarItem>
          {refreshButton}
        </ToolbarItem>
        <ToolbarItem>
          {pagination}
        </ToolbarItem>
      </ToolbarContent>
    </Toolbar>
  )

  const emptyTable = (
    <Bullseye>
      <EmptyState>
        <EmptyStateIcon icon={SearchIcon} />
        <Title headingLevel="h5" size="lg">
          No results found
        </Title>
        <EmptyStateBody>
          {'No results match this filter criteria. ' +
            'Clear all filters to show results.'}
        </EmptyStateBody>
        <EmptyStateSecondaryActions>
          <Button variant="link" onClick={() => clearAll()}>
            Clear all filters
          </Button>
        </EmptyStateSecondaryActions>
      </EmptyState>
    </Bullseye>
  )

  const table = (
    <StandardTable
      rows={filteredRows.slice((page - 1) * perPage, page * perPage)}
      columns={columns}
      emptyTable={emptyTable}
      isHoverable={props.onRowClick !== null}
      selectedRows={selectedRow}
      onRowClick={doRowClick}
    />
  )

  return (
    <React.Fragment>
      {toolbar}
      <Divider />
      {table}
    </React.Fragment>
  )
}

ResultsTable.propTypes = {
  resultsList: PropTypes.array.isRequired,
  scanTypes: PropTypes.array.isRequired,
  onRowClick: PropTypes.func.isRequired,
  selectedScan: PropTypes.string.isRequired,
  updateResultsList: PropTypes.func.isRequired,
  toggleMenu: PropTypes.func.isRequired,
  toggleResults: PropTypes.func.isRequired
}

export default ResultsTable
