import React from 'react'
import {
  Button,
  Toolbar,
  ToolbarContent,
  ToolbarItem
  // Text
} from '@patternfly/react-core'
import {
  AngleDownIcon,
  AngleRightIcon,
  SyncIcon
} from '@patternfly/react-icons'
// Screens
// Components
import {
  TypeaheadSelectInput,
  // StandardTable,
  Tree
} from '@itk-demo-sw/components'
import runkeyTreeElements from './runkey-tree-elements'
// Hooks
import {
  useTypeaheadSelectInput,
  useTree
} from '@itk-demo-sw/hooks'
import {
  generalRequest
} from '@itk-demo-sw/utility-functions'
// PropTypes
import PropTypes from 'prop-types'

const RunkeyView = (props) => {
  const [RunkeyTreeLayer, RunkeyTreePayload] = runkeyTreeElements()
  // States and custom Hooks
  // The Tree
  const [loadTree, toggleComp, tree, metaData, expandedMap, toggleAll, clearTree, findId] = useTree({ metaDataAttributes: ['type'], features: ['dynamic'], enablePayloads: false })
  // DropDownMenu to select a Runkey
  const [
    runkeyTag,
    isRunkeySelectOpen,
    onRunkeySelect,
    onRunkeyToggle,
    clearSelection
  ] = useTypeaheadSelectInput(null, (newRunkey) => {
    if (newRunkey !== null) {
      getRunkey(newRunkey)
    } else {
      clearTree()
    }
  })

  const getRunkey = (tagOrId) => {
    const reqBody = {
      stage: false,
      metadata: false,
      verbosity: 2,
      name: tagOrId[0]
    }
    generalRequest(
      `${props.viewerUrl}/db_get_tag`, reqBody
    ).then(
      data => {
        const root = { children: [], payload: [], id: tagOrId, type: 'runkey' }
        const hw = []
        for (const obj of Object.values(data.payload.objects)) {
          // Filter out all hardware trees
          for (let i = 0; i < obj.children.length; i++) {
            if (obj.children[i].type === 'felix') {
              hw.push(obj.children[i])
            }
          }
          // Add the hardware trees as child to every scan
          for (let i = 0; i < obj.children.length; i++) {
            if (obj.children[i].type === 'scan') {
              obj.children[i].children = hw
              root.children.push(obj.children[i])
            }
          }
        }
        console.log(root)
        console.log(findId(root, '1'))
        loadTree(root)
      }
    ).catch(
      err => {
        props.onError(err)
      }
    )
  }

  // React Components
  const toggleAllButton = (
    <Button
      onClick={toggleAll}
      isDisabled={Object.entries(tree).length === 0}
    >
      {Object.values(expandedMap).every(val => {
        return Object.values(val).every(v => v)
      })
        ? <AngleDownIcon />
        : <AngleRightIcon />}
      Expand/Collapse All
    </Button>
  )

  const runkeySelect = (
    <TypeaheadSelectInput
      clearSelection={clearSelection}
      onSelect={onRunkeySelect}
      onToggle={onRunkeyToggle}
      isOpen={isRunkeySelectOpen}
      selected={runkeyTag}
      selectOptions={props.runkeyList}
      placeholderText={'Select a runkey'}
      noValidate
    />
  )

  const refreshButton = (
    <Button
      variant={'plain'}
      onClick={() => props.updateRunkeys()}
    >
      <SyncIcon />
    </Button>
  )

  const runkeyTree = (
    <Tree
      tree={tree}
      expandedMap={expandedMap}
      toggleComp={toggleComp}
      metaData={metaData}
      createLevel={RunkeyTreeLayer}
      createLowestLevel={RunkeyTreePayload}
      onCompSelect={props.onCompSelect}
      isTitleButton={true}
    // createLowestLevel={(feContent, onClick, id) => {
    //   const getTextEntry = (type, id) => {
    //     const text = (
    //       <React.Fragment>
    //         <Text
    //           key={`type ${id}`}
    //           style={{
    //             fontSize: '16px',
    //             fontWeight: 'bold',
    //             display: 'inline-block'
    //           }}
    //         >
    //           {`${type}:`}
    //         </Text>
    //         {id && <Text
    //           key={`Id ${id}`}
    //           style={{
    //             fontSize: '16px',
    //             display: 'inline-block',
    //             whiteSpace: 'pre-wrap'
    //           }}
    //         >
    //           {` ${id}`}
    //         </Text>}
    //       </React.Fragment>)
    //     return text
    //   }
    //   return (props.results.map(res => <Button
    //     variant='plain'
    //     key={`TitleButton ${id}`}
    //     onClick={() => {
    //       if (onClick) {
    //         onClick(res[1])
    //       }
    //     }}
    //     className={'my-plain-button'}
    //   >
    //     <StandardTable
    //       columns={[]}
    //       rows={[[
    //         getTextEntry('Runkey ID', res[0]),
    //         getTextEntry('Scan ID', res[1]),
    //         getTextEntry('Scan Type', res[2])
    //       ]]}
    //     ></StandardTable>
    //   </Button>))
    // }}
    />
  )

  const toolbar = (
    <Toolbar
      id="config-list-toolbar"
      collapseListedFiltersBreakpoint="xl"
    >
      <ToolbarContent>
        <ToolbarItem>
          {toggleAllButton}
        </ToolbarItem>
        <ToolbarItem>
          {runkeySelect}
        </ToolbarItem>
        <ToolbarItem>
          {refreshButton}
        </ToolbarItem>
      </ToolbarContent>
    </Toolbar>
  )

  return (
    <React.Fragment>
      {toolbar}
      {runkeyTree}
    </React.Fragment>
  )
}

RunkeyView.propTypes = {
  viewerUrl: PropTypes.string.isRequired,
  onError: PropTypes.func.isRequired,
  runkeyList: PropTypes.array.isRequired,
  updateRunkeys: PropTypes.func.isRequired,
  onCompSelect: PropTypes.func.isRequired,
  results: PropTypes.array.isRequired
}

export default RunkeyView
