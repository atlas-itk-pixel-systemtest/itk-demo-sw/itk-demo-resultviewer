from itk_demo_resultviewer.helper_functions import create_valid_json, create_tbufferjson
from .state import State
from datetime import datetime, timedelta
from rcf_response import Info, Error
from connexion import request
from requests import post
import logging
import socket
import glob
import json
import os

logging.basicConfig(level=logging.INFO)

class Default(State):

  def __init__(self):
    self.name = "Default"
    self.headers = {'content-type': 'application/json', 'accept': 'application/json'}

  def db_get_tag(self,context,params):
    # body = {
    #   "stage": params["stage"],
    #   "name": params["name"],
    #   "verbosity": params["verbosity"],
    #   "metadata": params["metadata"],
    # }
    body = {}
    for key, entry in params.items():
      body[key] = entry
    
    result = post(
      f"{context.config_db_address}/tag/read",
      data=json.dumps(body),
      headers=self.headers
    ).json()
    if not result["status"] == 200:
      return Error(
        502,
        "Config DB Error",
        f"Error: Could not obtain object with tag "
        f"{params['name']} from database."
      )
    return Info(payload=result)

  def db_get_all(self,context,params):
    logging.debug(f"Sending get_all request to database at {context.config_db_address} for table {params['table']}")
    # body = {
    #   "verbosity" : params["verbosity"],
    #   "table" : params["table"],
    #   "stage" : params["stage"],
    #   "filter" : params["filter"],
    #   "child_filter" : params["child_filter"],
    #   "limit": params["limit"],
    #   "offset": params["offset"],
    #   "metadata": params["metadata"],
    #   "runkey_info": params["runkey_info"]
    #   }
    body = {}
    for key, entry in params.items():
      if key == "panel":
        continue
      body[key] = entry

    results_list = post(
      f"{context.config_db_address}/read_all",
      data=json.dumps(body),
      headers=self.headers
    ).json()

    if not results_list["status"] == 200:
      return Error(
        502,
        "Config DB Error",
        f"Error: Could not obtain scan result table "
        "from database."
      )
  
    if params["panel"] == "allResults":
      #Cut off the seconds and microseconds from timestamp
      #TODO: Add timezone
     for entry in results_list["list"]:
       timestamp = entry["metadata"]["time"]
       cuthere = timestamp.rfind(":")
       time_formatted = datetime.fromisoformat(timestamp)
       timestamp = timestamp[:cuthere]
       entry["metadata"]["time"] = timestamp
        #This should happen, but it happened anyways, so here is the failsave
       if "runkey_info" not in entry.keys():
        entry["runkey_info"] = {}
        entry["runkey_info"]["name"] = "-"
       
    return Info(payload=results_list)


  def cache_json(self,context,params):
    id = params["id"]
    logging.debug(f"Requesting information of scan {id} from database at {context.config_db_address}")
    ### Get list of results saved for requested scan ###
    body = {
      "stage": False,
      "id": id,
      "verbosity": 2
    }
    scan_res = post(
      f"{context.config_db_address}/object/read",
      data=json.dumps(body),
      headers=self.headers
    ).json()
    if not scan_res["status"] == 200:
      return Error(
        502,
        "Config DB Error",
        f"Error: Could not obtain scan result for ID "
        f"{id} from database."
    )
    all_results = {}
    logging.debug(f"Loading result data from scan {scan_res['id']} from database at {context.config_db_address}")
    ### Get data of results ###
    # Verbosity 3 needed to get the payload data
    body["verbosity"] = 3
    for scan_child in scan_res["children"]:
      # Filter out all children except for the histograms
      if (scan_child["type"] != "result" or "payloads" not in scan_child):
        continue

      for i, payload in enumerate(scan_child["payloads"]):
        if (payload["type"] != "histogram"):
          continue
        body["id"] = payload["id"]
        result_data = post(
          f"{context.config_db_address}/payload/read",
          data=json.dumps(body),
          headers=self.headers
        ).json()
        if not result_data["status"] == 200:
            return Error(
              502,
              "Config DB Error",
              f"Error: Could not obtain scan result for ID "
              f"{id} from database."
            )
        # Save all histograms for one FE in one list for further processing
        all_results[payload["id"]] = [] 
        all_results[payload["id"]].append(result_data["data"])


    jsonPath = "/dev/shm/resultscache/"   

    payload = {
      "jsonPaths": []
    }
    try:
      if not os.path.isdir(jsonPath):
        os.mkdir(jsonPath)
      for feid, histos in all_results.items():
        # check if the histogram files already exist, if not, create all files for the selected scan
        if not (glob.glob(jsonPath+"Result_"+feid+"__*.json")):
          logging.debug(f"Results for scan {feid} not found in {jsonPath}, creating the json files")
          for scan_result in histos:
            # yarr histo, replace this check by query on type info from db once this is possible
            if (scan_result.find("_typename") == -1 ):
              logging.debug("cache_json: Yarr result found")
              # Brackets are needed to get a proper json object
              scan_result = "["+scan_result+"]"
              results = create_tbufferjson(scan_result)
            else:   
              logging.debug("cache_json: Itk-felix-sw result found")
              results = create_valid_json(scan_result, "}{", "{", "}")
            for i in range(0, len(results)):
              data = json.loads(results[i])
              name = data["fName"].split("_FE")[0]
              #This is relevant for threshold scans only
              vcal = data["fTitle"].split("Delta ")
              if (len(vcal)>1):
                name = name+"__"+vcal[1]
              File = "Result_"+feid+"__"+name+"__.json"
              json_file = open(jsonPath+File, 'w')
              json_file.write(results[i])
              json_file.close()
              payload["jsonPaths"].append(File)
        else:
          logging.debug(f"Results for scan {feid} found in {jsonPath}")
          File = glob.glob(jsonPath+"Result_"+str(feid)+"*.json")
          File = [item.split(jsonPath)[1] for item in File]
          # For readability of itk-felix-sw threshold scan results list, 
          # otherwise all the occupancy maps are at the top
          File.reverse()
          for filename in File:
            payload["jsonPaths"].append(filename) 

      logging.debug(f"List of files: {payload['jsonPaths']}")          
      return Info(payload = payload)
    except:
      return Error(
        # TODO which number to use here?
        400,
        f"Error: Could not cache json file "
      )
