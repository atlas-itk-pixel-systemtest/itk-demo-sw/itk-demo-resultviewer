import React from 'react'
import {
  Page,
  PageSection,
  PageSectionVariants,
  Text
} from '@patternfly/react-core'
// Components
import PanelTitle from '../components/panel-title'
// Proptypes
import PropTypes from 'prop-types'

const FillerPanelNoAPI = (props) => {
  // React components
  const title = (
    <PanelTitle
      title={'Result Viewer'}
      subtext={'Display the histograms from scan results stored in the database '}
      dbAvailable={props.dbAvailable}
    />
  )

  const fillerPanel = (
    <PageSection
      isFilled
      variant={PageSectionVariants.light}
    >
    <Text
        component="p"
        style={{
          color: 'rgb(100,149,237,0.7)',
          fontSize: '22px',
          display: 'inline-block',
          whiteSpace: 'pre-wrap'
        }}
      >
        {'No response from the Api - is the container running? \n To upload and display local root files, use the "Display .root files" panel.'}
      </Text>
    </PageSection>
  )

  return (
    <Page>
    {title}
    {fillerPanel}
    </Page>
  )
}

FillerPanelNoAPI.propTypes = {
  dbAvailable: PropTypes.bool.isRequired
}

export default FillerPanelNoAPI
