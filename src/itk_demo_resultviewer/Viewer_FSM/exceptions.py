from rcf_response.exceptions import RcfException


class FSMException(RcfException):
    def __init__(self, msg, title="FSM Error", status=500, instance=None, ext=None):
        super().__init__(status, title, msg, instance=instance, ext=ext)


class InputError(FSMException):
    def __init__(self, msg):
        super().__init__(msg, status=400, title="Input Error")


class InternalError(FSMException):
    def __init__(self, msg):
        super().__init__(msg, status=500, title="Internal Server Error")


class ResultError(FSMException):
    def __init__(self, msg):
        super().__init__(msg, status=500, title="Result Error")


class StateError(FSMException):
    def __init__(self, msg):
        super().__init__(msg, status=500, title="State Error")

