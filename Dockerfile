
FROM python:3.10-slim

RUN apt-get update \
&& apt-get install -y --no-install-recommends git bash sudo curl nginx xz-utils\
&& apt-get purge -y --auto-remove \
&& rm -rf /var/lib/apt/lists/*

# ARG PYTHON_VERSION=3.9.6
# ARG USERNAME=demi
# ARG REPO=itk-demo-resultviewer

ENV HOME=/root
WORKDIR ${HOME}

# RUN git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/${REPO}.git ${HOME}/itk-demo-sw/${REPO}
# COPY . ${HOME}/itk-demo-sw/${REPO}

# WORKDIR ${HOME}/itk-demo-sw/${REPO}

# COPY ${WORKDIR}/root /
# COPY ${WORKDIR}/pyproject.toml /

# Add user nginx
RUN groupadd nginx
RUN useradd -m -r -g nginx nginx
RUN echo nginx ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/nginx \
    && chmod 0440 /etc/sudoers.d/nginx


RUN python3 -m pip install --no-cache-dir poetry
ENV PATH $HOME/.local/bin:${PATH}
RUN poetry config virtualenvs.in-project true

# COPY . .
COPY src ./src
COPY README.md .
COPY pyproject.toml .
COPY nginx_default.conf.template /etc/nginx/nginx.conf
RUN poetry install --without dev

COPY root /

#EXPOSE 80
EXPOSE 5010


#CMD poetry run gunicorn wsgi:app --bind 0.0.0.0:${RESULTVIEW_API_PORT:-5010} --workers=4 --timeout 600

ARG S6_OVERLAY_VERSION=3.1.5.0
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-x86_64.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-x86_64.tar.xz
ENTRYPOINT ["/init"]
ENV S6_STAGE2_HOOK="/init-hook"



