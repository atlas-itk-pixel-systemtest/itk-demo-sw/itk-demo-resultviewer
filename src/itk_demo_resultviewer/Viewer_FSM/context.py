from .default import Default
from .exceptions import InternalError

class Context:
  def __init__(self, config_db_address):
    self.state = Default()
    self.states = {
      "Default": Default
    }
    self.config_db_address = "" if config_db_address is None else config_db_address
        
  def set_state(self, stateName):
    print(f"Setting state to {stateName}")
    if stateName not in self.states:
      raise KeyError(f"State '{stateName}' not in list of available states.")
    self.state = self.states[stateName]



