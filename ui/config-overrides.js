const webpack = require('webpack');

module.exports = function override(config) {
    const fallback = config.resolve.fallback || {};
    Object.assign(fallback, {
        "crypto": require.resolve("crypto-browserify"),
        "stream": require.resolve("stream-browserify"),
        "assert": require.resolve("assert"),
        "http": require.resolve("stream-http"),
        "https": require.resolve("https-browserify"),
        "os": require.resolve("os-browserify"),
        "url": require.resolve("url"),
        "fs": require.resolve('browserify-fs'),
        "zlib": require.resolve("browserify-zlib"),
        "child_process": false,
        "net": false,
        "tls": false,
        "tmp": false,
        "xhr2": false,
        "path": require.resolve("path-browserify")
    })
    config.resolve.fallback = fallback;
    const experiments = config.experiments || {};
    Object.assign(experiments, {
        "topLevelAwait": true
    })
    config.experiments = experiments;
    config.plugins = (config.plugins || []).concat([
        new webpack.ProvidePlugin({
            process: 'process/browser.js',
            Buffer: ['buffer', 'Buffer']
        })
    ])
    return config;
}

