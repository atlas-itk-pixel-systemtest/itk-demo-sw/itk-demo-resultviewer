import React from 'react'

const MinimalRootPanel = () => {
  const serverUrl = '/jsroot/index.htm'
  const queryString = '?optstat=00&draw'

  return (
    <iframe
    className='jsroot-iframe'
    src={serverUrl + queryString}
    allowFullScreen/>
  )
}

export default MinimalRootPanel
