import React from 'react'
import {
  Text,
  TextContent,
  Flex,
  FlexItem
} from '@patternfly/react-core'
import PropTypes from 'prop-types'

const ResultView = (props) => {
  const serverUrl = '/jsroot/index.htm'
  const viewerUrlShortened = props.viewerUrl.replace('/api', '')
  const queryString = '?nobrowser&optstat=00&json=' + viewerUrlShortened + '/resultscache/'

  const iframes = []

  for (let i = 0; i < props.selectedMenuItems.length; i++) {
    iframes.push(<iframe
      className='full-iframe'
      src={serverUrl + queryString + props.curResultsPaths[props.selectedMenuItems[i]]} />)
  }

  return (
     <Flex direction={{ default: 'column' }}>
       <FlexItem>
        <TextContent>
          <Text
            style={{
              color: 'rgb(100,149,237,1)',
              fontSize: '28px',
              whiteSpace: 'pre-wrap'
            }}
          >
            {'Results View'}
          </Text>
        </TextContent>
      </FlexItem>
      <FlexItem>
        {iframes}
      </FlexItem>
    </Flex>
  )
}

ResultView.propTypes = {
  curResultsData: PropTypes.string.isRequired,
  curResultsTag: PropTypes.string.isRequired,
  curResultsType: PropTypes.string.isRequired,
  curResultsPaths: PropTypes.string.isRequired,
  selectedMenuItems: PropTypes.any.isRequired,
  viewerUrl: PropTypes.string.isRequired
}

export default ResultView
