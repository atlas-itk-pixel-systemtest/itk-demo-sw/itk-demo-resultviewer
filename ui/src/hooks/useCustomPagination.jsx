import { useState } from 'react'

export default function usePagination (
  initPerPage = 0,
  setselectedRow
) {
  const [page, setPage] = useState(1)
  const [perPage, setPerPage] = useState(initPerPage)

  const handleSetPage = (_evt, newPage, perPage) => {
    setPage(newPage)
    setPerPage(perPage)
  }

  const handlePerPageSelect = (_evt, newPerPage, newPage = 1) => {
    setPerPage(newPerPage)
    setPage(newPage)
  }

  const resetPage = (val = 1) => {
    setPage(val)
  }

  const onSetPage = (_event, pageNumber) => {
    setselectedRow([])
    handleSetPage(event, pageNumber, perPage)
  }

  const onFirstClick = (perPage) => {
    setselectedRow([])
    handleSetPage(event, 1, perPage)
  }
  const onNextClick = (page, perPage) => {
    setselectedRow([])
    handleSetPage(event, page + 1, perPage)
  }

  const onPreviousClick = (page, perPage) => {
    setselectedRow([])
    handleSetPage(event, page - 1, perPage)
  }

  const onLastClick = (itemCount, perPage) => {
    const lastPage = parseInt(itemCount / perPage) + 1
    setselectedRow([])
    handleSetPage(event, lastPage, perPage)
  }

  return [page, perPage, handlePerPageSelect, resetPage, onSetPage, onFirstClick, onNextClick, onPreviousClick, onLastClick]
}
