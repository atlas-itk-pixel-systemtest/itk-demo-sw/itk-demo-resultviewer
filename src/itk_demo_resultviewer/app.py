from connexion import FlaskApp
from connexion.exceptions import ProblemException
from flask_cors import CORS
from rcf_response import Error
from flask import redirect, request


def create_app(config=None):
    app = FlaskApp(
        __name__,
        specification_dir="openapi/",
        options={
            "swagger_ui": True,
            #"swagger_url": "/ui/",  # itkpixdaq
        },
        server_args={
 #           "static_folder": "../ui/build", # itkpixdaq
 #          "static_url_path": "/",  # itkpixdaq
        },
    )

    @app.route('/')
    def index():
        return redirect("/ui", code=308)

    flask_app = app.app
    CORS(flask_app)

    with app.app.app_context():
        app.add_api("openapi.yml")

#        @flask_app.route("/")    # itkpixdaq
#        def index(): # itkpixdaq
#           return flask_app.send_static_file("index.html")  # itkpixdaq

    app.add_error_handler(404, flask_error_response)
    app.add_error_handler(405, flask_error_response)
    app.add_error_handler(500, flask_error_response)
    app.add_error_handler(ProblemException, connexion_error_response)

    return app


def create_flask_app(config=None):
    app = create_app(config)

    return app.app


def flask_error_response(error):
    error = Error(error.code, error.name, error.description + f" ({request.url}).")
    return error.to_conn_response()


def connexion_error_response(error):
    error = Error(error.status, error.title, error.detail)
    return error.to_conn_response()

# adapted from http://flask.pocoo.org/snippets/35/
class ReverseProxied:
    """Wrap the application in this middleware and configure the
    reverse proxy to add these headers, to let you quietly bind
    this to a URL other than / and to an HTTP scheme that is
    different than what is used locally.

    In nginx:

    location /proxied/ {
        proxy_pass http://192.168.0.1:5001/;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Scheme $scheme;
        proxy_set_header X-Forwarded-Path /proxied;
    }

    :param app: the WSGI application
    :param script_name: override the default script name (path)
    :param scheme: override the default scheme
    :param server: override the default server
    """

    def __init__(self, app, script_name=None, scheme=None, server=None):
        self.app = app
        self.script_name = script_name
        self.scheme = scheme
        self.server = server

    def __call__(self, environ, start_response):
        script_name = environ.get("HTTP_X_FORWARDED_PATH", "") or self.script_name
        if script_name:
            environ["SCRIPT_NAME"] = "/" + script_name.lstrip("/")
            path_info = environ["PATH_INFO"]
            if path_info.startswith(script_name):
                environ["PATH_INFO_OLD"] = path_info
                environ["PATH_INFO"] = path_info[len(script_name) :]
        scheme = environ.get("HTTP_X_SCHEME", "") or self.scheme
        if scheme:
            environ["wsgi.url_scheme"] = scheme
        server = environ.get("HTTP_X_FORWARDED_SERVER", "") or self.server
        if server:
            environ["HTTP_HOST"] = server
        return self.app(environ, start_response)


#if __name__ == "__main__":
#    app = create_app()
#    host = "0.0.0.0"
#    port = 5010
#    app.run(host=host, port=port, debug=False)

