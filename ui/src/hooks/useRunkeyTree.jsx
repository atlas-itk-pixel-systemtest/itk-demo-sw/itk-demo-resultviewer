import { useState } from 'react'

/**
 * useRunkeyTree defines the stateful logic of the RunkeyTree
 * component.
 *
 * Params:
 *  levelNames: array - Array of string. Gives the Names of the
 *  displayable levels. Its length indirirectly defines what will
 *  be the lowest level. Levels are created for every element of
 *  levelNames everything that goes deeper will become the lowest
 *  level
 *
 * Returns:
 * [
 *  runkey: object - A runkey structured as an object. FELIXCards
 *  contain Optoboards contain Frontends contain their specifications
 *  expandedMap: object - The runkey structured in levels. Each
 *  component has a boolean that is true if it is expanded
 *  loadRunkey: function - Load a new runkey. And sets the states
 *  accordingly.
 *  toggleAll: function - Expands all component if atleast one is
 *  collapsed. If all components are expanded they collapse
 *  clearRunkey: function - Clears the runkey and the states
 *  internals: object - Internal variables used in the RunkeyTree.
 * ]
 */

export default function useRunkeyTree (
  levelNames = ['FELIX', 'Optoboard', 'FE']
) {
  const [runkey, setRunkey] = useState({})
  const [expandedMap, setDestructure] = useState({})
  const [lowestLevel, setLowestLevel] = useState({})
  const levels = levelNames.map((v, i) => i)

  const toggleComp = (name, id) => {
    const level = levelNames.indexOf(name)
    const newDestructure = { ...expandedMap }
    newDestructure[level][id] = !expandedMap[level][id]
    setDestructure(newDestructure)
  }

  const destructureRunkey = (runkey, levels) => {
    if (Object.keys(runkey).length === 0) return
    const expandedMap = {}
    const lowestLevel = {}
    const createRunkeyLayer = (obj, index, parentId) => {
      Object.entries(obj).forEach(([key, val]) => {
        const id = !parentId ? key : `${key}-${parentId}`
        if (levels.includes(index)) {
          if (!(index in expandedMap)) expandedMap[index] = {}
          expandedMap[index][id] = false
        }
        if (index === Math.max(...levels)) {
          lowestLevel[id] = val
        }
        createRunkeyLayer(val, index + 1, id)
      })
    }
    createRunkeyLayer(runkey, 0, '')
    return [expandedMap, lowestLevel]
  }

  const toggleAll = () => {
    const toggleTo = !Object.values(expandedMap).every(val => {
      return Object.values(val).every(v => v)
    })
    const newDestructure = { ...expandedMap }
    Object.values(newDestructure).forEach(val => {
      Object.keys(val).forEach(v => { val[v] = toggleTo })
    })
    setDestructure(newDestructure)
  }

  const clearRunkey = () => {
    setRunkey({})
    setDestructure({})
  }

  const loadRunkey = (tree) => {
    // Bring conn_list in required tree-format
    const newRunkey = {}
    for (const conn of Object.values(tree)) {
      if (!(`${conn.felix}` in newRunkey)) {
        newRunkey[conn.felix] = {}
      }
      if (!(`${conn.optoboard}` in newRunkey[conn.felix])) {
        newRunkey[conn.felix][conn.optoboard] = {}
      }
      const newConn = {}
      newConn.rx = conn.rx
      newConn.tx = conn.tx
      newConn.frontend = conn.frontend
      newConn.connectivity = conn.connectivity
      newRunkey[conn.felix][conn.optoboard][conn.frontend] = newConn
    }

    // Update expansion-  and toggle dicts
    const [dest, lowestLvl] = destructureRunkey(newRunkey, levels)
    setLowestLevel(lowestLvl)
    setDestructure(dest)

    // Set states
    setRunkey(newRunkey)
  }

  const internals = {
    lowestLevel,
    toggleComp,
    destructureRunkey,
    levelNames
  }

  return [
    runkey,
    expandedMap,
    loadRunkey,
    toggleAll,
    clearRunkey,
    internals
  ]
}
