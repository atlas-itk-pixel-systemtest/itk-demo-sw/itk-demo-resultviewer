import createConfig from 'react-runtime-config'

export const { useConfig, useAdminConfig } = createConfig({
  namespace: 'MY_APP_CONFIG',
  schema: {
    backend: {
      type: 'string',
      title: 'Backend URL/ prefix',
      description: 'Backend URL/ prefix',
      default: process.env.REACT_APP_BACKEND_URL || '/api'
      // default: '/api'
      // default: 'http://localhost:5010'
    },
    healthFrequency: {
      type: 'number',
      title: '/health call frequency [ms]',
      description: '/health call frequency [ms]',
      default: 10000
    },
    callHealth: {
      type: 'boolean',
      title: 'Call /health endpoint regularly',
      description: 'Call /health endpoint regularly',
      default: true
    }
  }
})
