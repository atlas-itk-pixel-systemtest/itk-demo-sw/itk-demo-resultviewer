import { useState } from 'react'

export default function useCustomCheckboxSelect (
  toggleMenu,
  toggleResults,
  setselectedRow
) {
  const [isOpen, setIsOpen] = useState(false)
  const [selections, setSelections] = useState([])

  const onToggle = () => setIsOpen(!isOpen)

  const clearSelections = () => {
    setSelections([])
    setIsOpen(false)
  }

  const onSelect = (event, selection) => {
    toggleMenu(false)
    toggleResults(false)
    setselectedRow([])
    setSelections(prevSelections => {
      return selections.includes(selection)
        ? prevSelections.filter(value => value !== selection)
        : [...prevSelections, selection]
    })
  }

  return [isOpen, selections, onToggle, onSelect, clearSelections]
}
