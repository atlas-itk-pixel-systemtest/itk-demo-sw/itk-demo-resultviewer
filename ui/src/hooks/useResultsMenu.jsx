import React, { useState } from 'react'
import { Menu, MenuContent, MenuList, MenuItem } from '@patternfly/react-core'

export default function useMenu (
  curResultsPaths
) {
  const [selectedItems, setSelectedItems] = useState([])
  const menuItems = []

  const niceNames = {
    occ: 'Occupancy Map',
    OccupancyMap: 'Occupancy Map',
    enable: 'Enable Mask',
    EnMask: 'Enable Mask',
    meantot: 'Mean ToT',
    sigmatot: 'Sigma ToT',
    meantot_1d: 'Mean ToT 1D',
    sigmatot_1d: 'Sigma ToT 1D',
    tot: 'ToT',
    tid: 'Trigger ID',
    ttag: 'Trigger Tag',
    ttagmap: 'Trigger Tag Map',
    bcid: 'BCID',
    bcidmap: 'BCID Map',
    scur: 'S-Curve',
    scur2D: 'S-Curve 2D',
    L1Dist: 'L1 Distribution'
  }

  const MenuCheckboxList = () => {
    /* eslint no-unused-vars: ["error", {"args": "after-used"}] */
    const onSelect = (event: React.MouseEvent<Element, MouseEvent>, itemId: number | string) => {
      // const item = itemId as number
      const item = Number(itemId)
      if (selectedItems.includes(item)) {
        setSelectedItems(selectedItems.filter(id => id !== item))
      } else {
        if (selectedItems.length === 4) {
          setSelectedItems(selectedItems)
          // TODO generate pop-up message here
        } else {
          setSelectedItems([...selectedItems, item])
        }
      }
    }

    for (let i = 0; i < curResultsPaths.length; i++) {
      const histName = String(curResultsPaths[i]).split('__')[1]
      // Used only for 'vcal=' in itk-felix-sw threshold scans
      let potentialSuffix = String(curResultsPaths[i]).split('__')[2]
      if (potentialSuffix === '.json') {
        potentialSuffix = ''
      }
      const fullhistName = histName + potentialSuffix
      console.log(fullhistName)
      menuItems.push(<MenuItem hasCheck itemId={i} isSelected={selectedItems.includes(i)}>
      {niceNames[histName] ? niceNames[histName] + ' ' + potentialSuffix : histName + potentialSuffix}
    </MenuItem>)
    }

    return (
      <Menu onSelect={onSelect} selected={selectedItems}>
        <MenuContent>
          <MenuList>
            {menuItems}
          </MenuList>
        </MenuContent>
      </Menu>
    )
  }
  return [selectedItems, setSelectedItems, MenuCheckboxList]
}
