# `itk-demo-resultviewer` 

## Overview

Containerized microservice for displaying histograms obtained from FrontEnd scans.

It consists of the following OCI images:

2. **`itk-demo-resultviewer-api`**
  Flask API server served by `gunicorn` to access the configDB.

3. **`itk-demo-resultviewer-ui`**

  React UI to use the API from a web browser.

## User Instructions

### Prerequisites

- [Working Docker installation](https://demi.docs.cern.ch/containers/docker/) with `docker compose` plug-in (or compatible runtime / compose tool).

The resultviewer can be used standalone, in this case, only the interactive jsroot panel will be available.
The other panels display results saved in the [`configDB`](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb) and therefore reqiure a running configDB to function.

|`itk-demo-resultviewer` Release tag | configDB version required |
|--|--|
| latest | latest |
| readout-stack_v0.1 | readout-stack_v0.1 |

The prerequisites can be checked with the `./bootstrap` script.

### Method 1: Run natively

```shell
./bootstrap       # check dependencies
./config          # write configured instance(s)
cd instance/itkpixdaq_itk-demo-resultviewer/
docker compose up -d # pull API image and start API container in the background
cd ../itkpixdaq_itk-demo-resultviewer-ui/
docker compose up -d # pull UI image and start UI container in the background
```

### Method 2: Run in a Deployment Stack
**Still under construction**
The simplest method to use these containers is via a deployment orchestration stack.
The resultviewer containers are part of the
`readout-stack` ([instructions](https://demi.docs.cern.ch/deployments/readout-stack/)).

#### Environment Variables

| Variable | Purpose |
|--|--|
| START_API | Start gunicorn+Flask API server 1=yes 0=no |
| START_NGINX | Start nginx 1=yes 0=no |

## Developers instructions

If you want to contribute to this repo ...

### Method 1: Local Developer setup (git clone)

1. Clone Git repository:

```shell
git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-resultviewer.git
cd itk-demo-resultviewer
```

2. Write configuration to `.env` files using `config` script:

```shell
./config
```

This writes a default config mostly initialized by the `config` script in the
[`defaults/` repository](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/defaults).
Also, `.env` and interpolated `docker-compose.yml` is written to a temporary `instance` folder within the repo directory
(orchestration scripts may write multiple instances in case multiple resultviewer containers should be started).

The container is completely managed by `docker compose` which can now be run without having to set further environment variables:

3. To check the instance environment is correct, run

```docker compose config```

4.

- To pull the images run

```docker compose pull```

- To build the image

If you want to build a local version of the image for development, simply do:

```shell
docker compose build
```

(Normally the images are built by the GitLab CI/CD (see `gitlab-ci.yml`).
The image should be flexible enough that all configuration is possible at runtime.
If this is not the case, please contact us!)

5. To start the resultviewer container

```docker compose up [-d]```

If you want to pass further environment variables (or override those in `.env`) you can do so on the cmdline:

```shell
CONFIGDB_ADDRESS="my_debug_setup_configDB" docker compose up [-d]
```

6. Arbitrary commands can now be run from the host in the running container:

```shell
docker compose exec api bash --login -c less .env
```
Shortcuts can be written to the taskfile `tasks` (see [`itk-demo-felix`](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix) for examples).



### Method 2: Use a Python virtual environment to run natively

Activate and use the venv:

```shell
poetry shell
poetry update
poetry install
```

Test the `itk-demo-resultviewer` CLI locally:

```itk-demo-resultviewer```

Run the API development server locally:

```flask resultviewer```

Start the [gunicorn](https://gunicorn.org/) web server locally:

```tasks serve```

The Flask Python app lives in the `src/itk_demo_resultviewer` package directory.

### Modifying the configuration and start-up procedure

Configuration is injected at runtime via

- `docker-compose.yml` directly (local hacks only - better implement a variable in `config`).
- `./config` (which writes `.env`).
- Environment variables which can override certain values in the scripts (check inside).

s6-init

- The `s6-init` directory structure is copied from `./root` to `/` in the container during image building.
- The `s6-rc` script to configure and start nginx is at `root/etc/s6-overlay/s6-rc.d/nginx/run`.
- The `s6-rc` script to start `gunicorn` is at `root/etc/s6-overlay/s6-rc.d/svc-gunicorn/run`.

### Flask API server app

Frontend and API communicate via a REST API described in
[OpenAPI 3.0](https://swagger.io/docs/specification/about/).

The API description file is dynamically converted to Flask routes using the
spec-first Python module [connexion](https://github.com/zalando/connexion).

The Flask app was originally adapted from [this blogpost](https://mark.douthwaite.io/getting-production-ready-a-minimal-flask-app/).

The app communicates with the configDB through calls in the backend.

### Frontend

The frontend is a completely separate image (and container ) in the `ui/` subdirectory.

It is based on `create-react-app` and uses Patternfly 4 (which is based
on Bootstrap 4) components and layouts.

Links:
[JavaScript](https://www.w3schools.com/js/DEFAULT.asp) |
[React.js](https://reactjs.org/) |
[Patternfly](https://www.patternfly.org/v4/) |
[Bootstrap](https://getbootstrap.com/)

To develop locally:

1. Install and build the React toolchain based on `create-react-app`

```shell
cd ui/
nvm use stable
npm i
npm run build
```

2. You can test the transpiled static React app alone using [`serve`](https://www.npmjs.com/package/serve):

```shell
serve -s build
```

### Tagging and publishing procedure

1. Update CHANGELOG.md
2. Commit last changes for tag
3. Tag repository with a full SEMVER tag. This SEMVER tag needs to be PEP-440 compatible to be able to be used with Python packages as well!

```shell
git tag <MAJOR>.<MINOR>.<PATCH>
```

4. Push tag

```shell
git push origin <MAJOR>.<MINOR>.<PATCH>
```

5. The CI will

- publish all packages using the Git tag as package version to the [package registry](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-resultviewer/-/packages)
- build all images and tag using the Git tag as image tag and push to the [container registry](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-resultviewer/container_registry)

Questions: <maren.stratma@cern.ch>
