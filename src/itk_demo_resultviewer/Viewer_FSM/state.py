from rcf_response import Error, Info

class State:
  def __init__(self):
    self.name = ""

  def __repr__(self):
    return f"<State object - {self.name}>"

  def _undefinedError(self, endpoint=""):
    if endpoint == "":
      error_text = (
        f"This endpoint has not been implemented "
        f"for the current state '{self.name}'."
      )
    else:
      error_text = (
        f"The '{endpoint}' endpoint has not been implemented "
        f"for the current state '{self.name}'."
      )
    return Error(500, "State Error", error_text)

  def db_get_tag(self,context,params):
    return self._undefinedError()

  def db_get_all(self,context,params):
    return self._undefinedError()
  
  def cache_json(self,context,params):
    return self._undefinedError()






