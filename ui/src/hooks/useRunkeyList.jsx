import { useState, useEffect } from 'react'
import { generalRequest } from '@itk-demo-sw/utility-functions'

export default function useRunkeyList (
  viewerUrl,
  dbAvailable,
  onError = (data) => {}
) {
  const [runkeyList, setRunkeyList] = useState([])

  const reqBody = {
    panel: 'runkey',
    stage: false,
    limit: 100,
    offset: 0,
    filter: 'runkey',
    metadata: false,
    runkey_info: false,
    table: 'tag',
    verbosity: 0
  }

  const updateRunkey = () => {
    if (dbAvailable) {
      console.log('Updating runkey list')
      generalRequest(
        `${viewerUrl}/db_get_all`, reqBody
      ).then(
        data => {
          console.log(data)
          const newRunkeyList = []
          for (const [i, entry] of data.payload.list.entries()) {
            console.log(i)
            newRunkeyList.push([
              entry.name
            ])
          }
          setRunkeyList(newRunkeyList)
        }
      ).catch(
        err => onError(err)
      )
    } else {
      setRunkeyList([])
    }
  }

  useEffect(() => {
    updateRunkey()
  }, [viewerUrl, dbAvailable])

  return [runkeyList, updateRunkey]
}
