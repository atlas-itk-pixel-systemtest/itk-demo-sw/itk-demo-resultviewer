import React, { useState } from 'react'
import {
  Text,
  DataListContent,
  DataList,
  DataListItem,
  DataListItemRow,
  DataListToggle,
  DataListItemCells,
  DataListCell,
  Button
} from '@patternfly/react-core'
import { StandardTable } from '@itk-demo-sw/components'

export default function runkeyTreeElements () {
  const [scanId, setscanId] = useState('')

  const RunkeyTreeLayer = (id, metaData, content, expandedMap, toggleComp, onCompSelect, isTitleButton) => {
    const isExpanded = expandedMap[id]
    if (metaData[id].type === 'scan') {
      setscanId(id)
    }
    return (
      <DataListItem
        aria-label={`List ${id}`}
        isExpanded={isExpanded}
        key={`List ${id}`}
      >
        <DataListItemRow
          key={`Item Row ${id}`}
        >
          <DataListToggle
            onClick={() => toggleComp(id)}
            isExpanded={isExpanded}
            key={`List Toggle ${id}`}
          />
          <DataListItemCells
            style={{
              height: '40px'
            }}
            key={`Item Cell ${id}`}
            dataListCells={[
              <DataListCell key={`Name Cell ${id}`}>
                <Text
                  key={`Row Title ${id}`}
                  style={{
                    fontSize: '14px',
                    color: 'black'
                  }}
                >
                  <span style={{ color: 'navy', fontWeight: 'bold' }}>{metaData[id].type}:</span>  {id}
                </Text>
              </DataListCell>
            ]}
          />
        </DataListItemRow>
        <DataListContent
          key={`List Content ${id}`}
          isHidden={!isExpanded}
        >
          <DataList
            isCompact
            key={`${id}`}
          >
            {content}
          </DataList>
        </DataListContent>
      </DataListItem>)
  }

  const RunkeyTreePayload = (id, metaData, onCompSelect) => {
    return (
      <StandardTable
        columns={[]}
        rows={[[
          <Button
            variant='plain'
            key={`TitleButton ${id}`}
            onClick={() => {
              // console.log(scanId)
              onCompSelect(scanId)
            }}
          >
            <Text
              key={`type ${id}`}
              style={{
                fontSize: '14px',
                display: 'inline-block'
              }}
            >
              {`${metaData[id].type}:  ${id}`}
            </Text>
          </Button>
        ]]}
      />
    )
  }

  return [RunkeyTreeLayer, RunkeyTreePayload]
}
