import React from 'react'
import ReactDOM from 'react-dom/client'
import '@patternfly/react-core/dist/styles/base.css'
import './style.css'
import ResultsDashboard from './screens/dashboard'

const root = ReactDOM.createRoot(document.getElementById('root'))

root.render(
  <div className="fill-window">
    <ResultsDashboard/>
  </div>
)
