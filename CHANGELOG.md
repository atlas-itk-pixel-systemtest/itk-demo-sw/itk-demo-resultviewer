# Changelog

All notable changes[^1] to the `itk-demo-resultviewer` monorepo.

This repo is "living at head" which means tags correspond to the overall repo.
All subrepos/workspaces, all packages in the package registry and all container images in the image registry follow the repo tag.

[^1]: The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

(always keep this section to take note of untagged changes at HEAD)

### Changed

- The resultviewer is restructured to work with the new configDB. The allResults panel works, the Runkey panel is still under construction.


## [readout-stack_v0.1] - 2023-06-14
- Only tag before starting changelog.
- This is the version of the resultviewer that works with the readout-stack v.0.1. The configDB has undergone major changes which include changes to the API, therefore this version of the resultviewer is not compatible with the current readout-stack!


[readout-stack_v0.1]: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-resultviewer/-/tree/readout-stack_v0.1?ref_type=tags
