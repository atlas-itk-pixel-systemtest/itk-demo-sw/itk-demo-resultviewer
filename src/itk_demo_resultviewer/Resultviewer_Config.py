import os
from config_checker import BaseConfig,EnvSource


class resulviewer_config(BaseConfig):
    start_nginx : str = None
    etcd_host : str = "localhost"
    etcd_port : int = 2379
    etcd_namespace : str = "demi"
    configdb_adress : str = 'http://localhost:5000/api'
    configdb_url_key : str = None

    CONFIG_SOURCES = [EnvSource(allow_all=True,file=".env")]
 
START_NGINX = resulviewer_config().start_nginx

ETCD_HOST = resulviewer_config().etcd_host
ETCD_PORT = resulviewer_config().etcd_port
ETCD_NAMESPACE = resulviewer_config().etcd_namespace

CONFIGDB_ADDRESS = resulviewer_config().configdb_adress
CONFIGDB_URL_KEY = resulviewer_config().configdb_url_key
