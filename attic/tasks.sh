#!/bin/bash
#
# ITk demonstrator taskfile
#
# 2022, Gerhard Brandt <gbrandt@cern.ch>
#

script_dir="$(dirname "$0")"
alias run=./tasks.sh

# setup local environment if not already done
[ -f .env ] || ./config.sh
. .env

# DEFAULT_API_PORT="5010"
# DEFAULT_UI_PORT="5082"

# export GITLAB_PROJECTID=
# export GITLAB_PROJECT=itk-demo-resultviewer

# export GITLAB_SERVER=gitlab.cern.ch
# export GITLAB_ROOTGROUP=atlas-itk-pixel-systemtest
# export GITLAB_SUBGROUP=itk-demo-sw
# # export GITLAB_FEATURE=feature/my-feature-0

# export GITLAB_REGISTRY=gitlab-registry.cern.ch
# export GITLAB_REGISTRY_IMAGE=${GITLAB_REGISTRY}/${GITLAB_ROOTGROUP}/${GITLAB_SUBGROUP}/${GITLAB_PROJECT}

# echo "image: ${GITLAB_REGISTRY_IMAGE}"

# # GITLAB_TOKEN=$(cat ./.gitlab-token)
# # export GITLAB_TOKEN

# export NAMESPACE="demi.${GITLAB_PROJECT}"
# PUID=$(id -u)
# PGID=$(id -g)
# PPWD=$(pwd)
# export PUID PGID PPWD PORT


# . "$(poetry env list --full-path | grep Activated | cut -d' ' -f1 )/bin/activate"


# function obtain_settings {
#   export USE_REGISTRY_CACHE=${USE_REGISTRY_CACHE:-1}
#   export ETCD_HOST=${ETCD_HOST:-$(hostname)}
#   export ETCD_PORT=${ETCD_PORT:-2379}
#   export API_HOST=${API_HOST:-$(hostname)}
#   export API_WORKERS=4
#   export UI_HOST=${UI_HOST:-$(hostname)}
#   if [ -z ${API_PORT+x} ]; then
#     if ! API_PORT=$(registry_cli allocate_port -p ${DEFAULT_API_PORT} -eh ${ETCD_HOST} -ep ${ETCD_PORT}); then
#       API_PORT=-1
#     fi
#   fi
#   if [ ! -z ${API_PORT+x} ]; then
#     export API_PORT
#   fi
#   if [ -z ${UI_PORT+x} ]; then
#     if ! UI_PORT=$(registry_cli allocate_port -p ${DEFAULT_UI_PORT} -eh ${ETCD_HOST} -ep ${ETCD_PORT}); then
#       UI_PORT=-1
#     fi
#   fi
#   if [ ! -z ${UI_PORT+x} ]; then
#     export UI_PORT
#   fi
# }

# function write_cache {
#   cat <<EOF >${script_dir}/.registry_cache
# export API_HOST=${API_HOST}
# export API_PORT=${API_PORT}
# export UI_HOST=${UI_HOST}
# export UI_PORT=${UI_PORT}
# export NAME=${NAME}
# EOF
# }

# function read_cache {
#   source ${script_dir}/.registry_cache 2>/dev/null
# }

function register_resultviewer {
  obtain_settings
  if [ "$USE_REGISTRY_CACHE" -eq 1 ]; then
    echo "Reading registry information from cache"
    read_cache
  else
    echo "Registering new service"
  fi
  cmd="registry_cli register_service "
  if [ ! -z ${NAME+x} ]; then
    cmd+=" -n ${NAME}"
  fi
  cmd+=" -eh ${ETCD_HOST}"
  cmd+=" -ep ${ETCD_PORT}"
  cmd+=" -h ${API_HOST}"
  cmd+=" itk-demo-resultviewer"
  cmd+=" category Microservices"
  cmd+=" port ${API_PORT}"
  cmd+=" protocoll http"
  cmd+=" health /health"
  cmd+=" apiPrefix /api"
  cmd+=" gui http://${UI_HOST}:${UI_PORT}"
  cmd+=" ui_host ${UI_HOST}"
  cmd+=" ui_port ${UI_PORT}"
  if NAME=$($cmd); then
    NAME=${NAME#*Service name: }
    NAME=${NAME%Hostname*}
    NAME=${NAME//[$'\t\r\n']}
    export NAME
    write_cache
  else
    echo "Failed to register service."
  fi
}

# Local tasks

function install {
  echo "Installing locally"
  export PYTHON_KEYRING_BACKEND=keyring.backends.fail.Keyring
  poetry config virtualenvs.in-project true
  poetry install
}

function api {
  gunicorn wsgi:app --bind 0.0.0.0:${API_PORT}
  # --log-level=debug
  # --access-logfile -
  # --workers=`nproc`
}

# Docker tasks

function all {
  # build
  # up
  help
}

progress="--progress auto"

function build {
  echo "Building Docker image"
  docker build -t ${API_IMAGE} . ${progress}
}

function rebuild {
  docker build -t ${API_IMAGE} . ${progress} --no-cache
}

function push {
  docker login ${GL_REGISTRY}
  docker push ${API_IMAGE}
}

function pull {
  docker login ${GL_REGISTRY}
  docker pull ${API_IMAGE}
  # docker tag ${GITLAB_REGISTRY_IMAGE}:latest ${GITLAB_PROJECT}
}

function run {
  docker run --rm -it ${API_IMAGE} bash
  # docker run --rm -it ${GITLAB_PROJECT} bash
}

function exec {
  docker exec -it ${API_IMAGE} ${1-bash}
  docker exec -it ${GL_PROJECT} ${1-bash}
}

function ps {
  docker compose ps
}

function logs {
  docker compose logs
}

function config {
  docker compose config
}

#shellcheck disable=SC2120
function up {
  register_resultviewer
  docker network inspect demi >/dev/null 2>&1 || {
    echo "Creating Docker network 'demi'"
    docker network create demi
  }
  docker compose up $1 
}

#shellcheck disable=SC2120
function down {
  docker compose down $1
}

function recompose {
  down
  up
}

function help {
  echo "usage: $0 <task> <args>"
  # declare -F | awk '{print $NF}' | sort | egrep -v "^_"
  compgen -A function | cat -n
  exit 0
}

"${@:-all}"
