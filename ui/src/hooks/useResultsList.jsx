import { useState, useEffect } from 'react'
import { generalRequest } from '@itk-demo-sw/utility-functions'

export default function useResultsList (
  viewerUrl,
  dbAvailable,
  onError = (data) => {}
) {
  const [resultsList, setResultsList] = useState([])

  const reqBody = {
    panel: 'allResults',
    stage: false,
    limit: 100,
    offset: 0,
    table: 'object',
    filter: 'scan',
    child_filter: 'result',
    metadata: true,
    runkey_info: true,
    verbosity: 1
  }

  const niceTypes = {
    digitalscan: 'DigitalScan',
    analogscan: 'AnalogScan',
    thresholdscan: 'ThresholdScan',
    testscan: 'TestScan',
    sensorscan: 'SensorScan',
    configscan: 'ConfigScan',
    totscan: 'TotScan',
    preamptune: 'PreampTune',
    maskcheck: 'MaskCheck',
    globalthresholdtune: 'GlobalThresholdTune',
    pixelthresholdtune: 'PixelThresholdTune'
  }

  // Get list of all results in the database
  const updateResults = () => {
    if (dbAvailable) {
      generalRequest(
      `${viewerUrl}/db_get_all`, reqBody
      ).then(
        data => {
          console.log(data)
          const newResultsList = []
          for (const [i, entry] of data.payload.list.entries()) {
            console.log(i)
            newResultsList.push([
              entry.runkey_info.name,
              entry.id,
              niceTypes[entry.metadata.scan_type] || '-',
              entry.metadata.time
            ])
          }
          setResultsList(newResultsList)
        }
      ).catch(
        err => onError(err)
      )
    } else {
      setResultsList([])
    }
  }

  useEffect(() => {
    updateResults()
  }, [viewerUrl, dbAvailable])

  return [resultsList, updateResults]
}
